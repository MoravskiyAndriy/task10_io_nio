package com.moravskiyandriy;

//package com.moravskiyandriy.serialization.genericship;

//import com.moravskiyandriy.serialization.droid.Droid;
//import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;

public class Comments {
    private static final Logger logger = LogManager.getLogger(Comments.class);

    /**
     * java-doc comment
     * some line 1
     * some line 2
     * some line 3
     */
    Comments() {
        //code comment
        int a = 0;
        int b = 0;
        //int c = 'b';
    }

    public void method() {
        int d = 5;
        int s = 9;
    }

    /*public void massComment(){
        int y=0;
        int r=0;
    }*/
}
