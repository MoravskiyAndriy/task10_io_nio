package com.moravskiyandriy.commands;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private CommandSwitcher commandSwitcher;
    private List<String> menu = new ArrayList<>();

    public Menu() {
        commandSwitcher = new CommandSwitcher();
        commandSwitcher.addCommand(new SerializeDeserializeCommand());
        menu.add("0. Serialize/deserialize");
        commandSwitcher.addCommand(new TestBufferedReadersCommand());
        menu.add("1. Test BufferedReaders");
        commandSwitcher.addCommand(new TestInputStreamCommand());
        menu.add("2. Test custom InputStream");
        commandSwitcher.addCommand(new ReadCommentsFromFileCommand());
        menu.add("3. Read comments from file.");
        commandSwitcher.addCommand(new TestDisplayContentCommand());
        menu.add("4. Display directory content.");
        menu.add("Type 'q' to quit.");
    }

    public void chose(final Integer number) {
        commandSwitcher.execute(number);
    }

    public void printMenu() {
        for (String s : menu) {
            System.out.println(s);
        }
    }
}
