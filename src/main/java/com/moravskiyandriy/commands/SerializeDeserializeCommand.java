package com.moravskiyandriy.commands;

import com.moravskiyandriy.serialization.serializationlogic.SerializationExample;

public class SerializeDeserializeCommand implements Command {
    public void execute() {
        new SerializationExample().serializeDeserialize();
    }
}
