package com.moravskiyandriy.commands;

import com.moravskiyandriy.bufferedreader.BuffedReaderTest;

public class TestBufferedReadersCommand implements Command {
    public void execute() {
        new BuffedReaderTest().TestBufferedReaders();
    }
}
