package com.moravskiyandriy.commands;

import java.util.ArrayList;
import java.util.List;

class CommandSwitcher {
    private List<Command> commandList = new ArrayList<>();

    CommandSwitcher() {
    }

    void addCommand(final Command command) {
        commandList.add(command); // optional
    }

    void execute(final Integer number) {
        commandList.get(number).execute();
    }
}
