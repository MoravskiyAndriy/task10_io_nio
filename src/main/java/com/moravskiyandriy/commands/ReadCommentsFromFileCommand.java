package com.moravskiyandriy.commands;

import com.moravskiyandriy.commentsreader.CommentsReader;

public class ReadCommentsFromFileCommand implements Command {
    public void execute() {
        new CommentsReader().ReadComments();
    }
}
