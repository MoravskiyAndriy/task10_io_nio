package com.moravskiyandriy.commands;

import com.moravskiyandriy.inputstream.CustomInputStreamTest;

public class TestInputStreamCommand implements Command {
    public void execute() {
        new CustomInputStreamTest().testCustomInputStream();
    }
}
