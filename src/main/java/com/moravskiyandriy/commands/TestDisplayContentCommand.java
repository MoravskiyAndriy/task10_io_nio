package com.moravskiyandriy.commands;

import com.moravskiyandriy.directorycontent.DisplayDirectoryContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class TestDisplayContentCommand implements Command {
    private static final Logger logger = LogManager.getLogger(TestDisplayContentCommand.class);

    public void execute() {
        DisplayDirectoryContent manager = new DisplayDirectoryContent();
        manager.getBasePath();
        manager.displayCurrentDirectoryContent();
        while (true) {
            Scanner input = new Scanner(System.in);
            logger.info("Input path to directory or 'q' to quit: ");
            String path = input.nextLine();
            if (path.equals("q")) {
                break;
            } else {
                manager.setCurrentDirectory(path);
                manager.displayCurrentDirectoryContent();
            }
        }
    }
}
