package com.moravskiyandriy.inputstream;

import com.moravskiyandriy.inputstream.customstream.CustomInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomInputStreamTest {
    private static final Logger logger = LogManager.getLogger(CustomInputStreamTest.class);
    private static final int PUSH_BACK_SIZE = 10;

    public void testCustomInputStream() {
        String data = "This an example of CustomInputStream";

        ByteArrayInputStream byteArrayInputStream = null;
        CustomInputStream CustomInputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(data.getBytes());
            CustomInputStream = new CustomInputStream(byteArrayInputStream, 128);

            List<Integer> list = new ArrayList<>();
            StringBuilder line = new StringBuilder();

            for (int i = 0; i < PUSH_BACK_SIZE; i++) {
                int c = CustomInputStream.read();
                list.add(c);
                line.append((char) c);
            }
            logger.info(line.toString());

            Collections.reverse(list);
            for (int item : list) {
                CustomInputStream.unread(item);
            }

            byte b[] = new byte[data.getBytes().length];
            CustomInputStream.read(b);
            logger.info(new String(b));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (CustomInputStream != null) {
                    CustomInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
