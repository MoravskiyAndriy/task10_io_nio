package com.moravskiyandriy.nioclientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class Client {
    private static final Logger logger = LogManager.getLogger(Client.class);
    private static final int PORT = 1111;
    private static final String HOSTNAME = "localhost";


    public static void main(String[] args) throws IOException {
        int port = 1111;
        InetSocketAddress addr = new InetSocketAddress(HOSTNAME, PORT);
        SocketChannel client = SocketChannel.open(addr);

        logger.info("Connecting to Server on port " + port + "...");
        boolean flag = true;

        while (flag) {
            Scanner input = new Scanner(System.in);
            logger.info("\n1- Type message.\n2- Read messages.");
            int choice = input.nextInt();
            input.nextLine();
            if (choice == 1) {
                logger.info("user number: ");
                String username = input.nextLine();
                logger.info("Input message or 'q' to quit: ");
                String msg = input.nextLine();

                byte[] message = (username + " " + msg).getBytes();
                ByteBuffer buffer = ByteBuffer.wrap(message);
                client.write(buffer);

                logger.info("sending: " + msg);
                buffer.clear();
                if (msg.equals("q")) {
                    client.close();
                }
            } else {
                ByteBuffer backBuffer = ByteBuffer.allocate(256);
                client.read(backBuffer);
                String result = new String(backBuffer.array()).trim();
                backBuffer.clear();
                logger.info("Message received from server: " + result);
            }
        }
    }
}