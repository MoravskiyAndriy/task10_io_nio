package com.moravskiyandriy.nioclientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

public class Server {
    private static final Logger logger = LogManager.getLogger(Server.class);
    private static final int PORT = 1111;
    private static final String HOSTNAME = "localhost";

    public static void main(String[] args) throws IOException {
        List<SocketChannel> users = new ArrayList<>();
        String tempMessage;
        Selector selector = Selector.open();

        ServerSocketChannel socket = ServerSocketChannel.open();
        InetSocketAddress addr = new InetSocketAddress(HOSTNAME, PORT);

        socket.bind(addr);
        socket.configureBlocking(false);

        int ops = socket.validOps();
        SelectionKey selectionKey = socket.register(selector, ops, null);

        while (true) {

            logger.info("server is waiting...");
            selector.select();

            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();

            while (iterator.hasNext()) {
                SelectionKey myKey = iterator.next();
                if (myKey.isAcceptable()) {
                    SocketChannel client = socket.accept();
                    users.add(client);
                    client.configureBlocking(false);
                    client.register(selector, SelectionKey.OP_READ);
                    logger.info("Connection Accepted: " + client.getLocalAddress() + "\n");

                } else if (myKey.isReadable()) {
                    SocketChannel client = (SocketChannel) myKey.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(256);
                    client.read(buffer);
                    String result = new String(buffer.array()).trim();

                    logger.info("Message received: " + result);
                    tempMessage = result;

                    if (result.contains("goodbye") || result.contains("qq")) {
                        client.close();
                        logger.info("\ngoodbye :)");
                    } else {
                        int user = Integer.valueOf(result.split(" ")[0]);
                        SocketChannel thisUser = users.get(user);
                        thisUser.register(selector, SelectionKey.OP_WRITE);
                        byte[] message = ("| MSG from " + users.indexOf(client) + ": " + tempMessage).getBytes();
                        ByteBuffer newBuffer = ByteBuffer.wrap(message);
                        thisUser.write(newBuffer);
                        logger.info("sending msg... ");
                        buffer.clear();
                        thisUser.register(selector, SelectionKey.OP_READ);
                    }
                }
                iterator.remove();
            }
        }
    }
}
