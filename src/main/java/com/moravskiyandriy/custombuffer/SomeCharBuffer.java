package com.moravskiyandriy.custombuffer;

import java.nio.BufferUnderflowException;

public class SomeCharBuffer implements BaseCharBuffer {
    final char[] array;
    private int mark = -1;
    private int position = 0;
    private int limit;
    private int capacity;
    final int offset;

    private class CapacityException extends Exception{
        CapacityException(String msg){
            super(msg);
        }
    }
    private class LimitException extends Exception{
        LimitException(String msg){
            super(msg);
        }
    }

    public SomeCharBuffer(int mark, int pos, int lim, int cap, char[] charArray, int offset) throws CapacityException{       // package-private
        if (cap < 0)
            throw new CapacityException("cap<0!");
        this.capacity = cap;
        limit=lim;
        position=pos;
        if (mark >= 0) {
            if (mark > pos)
                throw new IllegalArgumentException("mark > position: ("
                        + mark + " > " + pos + ")");
            this.mark = mark;
        }
        this.array=charArray;
        this.offset = offset;
    }

    public SomeCharBuffer(int capacity)throws CapacityException{
        if (capacity < 0)
            throw new CapacityException("cap<0!");
        this.capacity = capacity;
        this.limit=capacity;
        this.array=new char[capacity];
        this.offset=0;
    }

    public SomeCharBuffer(int mark, int pos, int lim, int cap) throws CapacityException
    {
        this(mark, pos, lim, cap, null, 0);
    }

    Object base() {
        return array;
    }

    final int nextGetIndex() {                          // package-private
        if (position >= limit)
            throw new BufferUnderflowException();
        return position++;
    }

    protected int ix(int i) {
        return i + offset;
    }

    public char get() {
        return array[ix(nextGetIndex())];
    }

    /*public int read(CharBuffer target) throws IOException {
        // Determine the number of bytes n that can be transferred
        int targetRemaining = target.remaining();
        int remaining = remaining();
        if (remaining == 0)
            return -1;
        int n = Math.min(remaining, targetRemaining);
        int limit = limit();
        // Set source limit to prevent target overflow
        if (targetRemaining < remaining)
            limit(position() + n);
        try {
            if (n > 0)
                target.put(this);
        } finally {
            limit(limit); // restore real limit
        }
        return n;
    }*/

    public SomeCharBuffer limit(int newLimit) throws LimitException{
        if (newLimit > capacity | newLimit < 0)
            throw new LimitException("newLimit is improper");
        limit = newLimit;
        if (position > limit) position = limit;
        if (mark > limit) mark = -1;
        return this;
    }

    @Override
    public void put(char value) {

    }

}
