package com.moravskiyandriy.custombuffer;

public interface BaseCharBuffer {
    public void put(char value);

    public char get();
}
