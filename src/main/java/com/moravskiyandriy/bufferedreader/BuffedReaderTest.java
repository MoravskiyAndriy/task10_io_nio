package com.moravskiyandriy.bufferedreader;

import com.moravskiyandriy.Application;
import com.moravskiyandriy.serialization.serializationlogic.SerializationExample;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Stream;

public class BuffedReaderTest {
    private static final Logger logger = LogManager.getLogger(Application.class);
    private static final String BUFFER_FILE_NAME = "bufferFile.txt";

    public void TestBufferedReaders() {
        Stream.iterate(1, n -> n * 2)
                .limit(10)
                .forEach(x -> testBReader(x));
    }

    private void testBReader(int size) {
        try {
            Properties prop = getProperties();
            FileReader in = new FileReader(Optional.ofNullable(prop.
                    getProperty("BUFFER_FILE_NAME")).
                    orElse(BUFFER_FILE_NAME));
            BufferedReader br = new BufferedReader(in, size);

            long startTime = System.currentTimeMillis();
            String line;
            while ((line = br.readLine()) != null) {
                line = "";
            }
            BigDecimal result = new BigDecimal(System.currentTimeMillis() - startTime).divide(new BigDecimal(1000));
            logger.info("Time consumed for size " + size + ": " + result + " s");
            in.close();
        } catch (FileNotFoundException e) {
            logger.info("File not found.");
        } catch (IOException e) {
            logger.info("IO exception found:");
            e.printStackTrace();
        }
    }

    private Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = SerializationExample.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
