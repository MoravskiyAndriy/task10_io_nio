package com.moravskiyandriy.directorycontent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class DisplayDirectoryContent {
    private static final Logger logger = LogManager.getLogger(DisplayDirectoryContent.class);
    private static final String PATH_VALUE = "C:/Users/1/IdeaProjects/IONIO/src/main/java/com/moravskiyandriy";
    private String currentDirectory;

    public void getBasePath() {
        Properties prop = getProperties();
        currentDirectory = Optional.ofNullable(prop.
                getProperty("PATH_VALUE")).
                orElse(PATH_VALUE);
    }

    public void displayCurrentDirectoryContent() {
        logger.info(currentDirectory + ":");
        File f = new File(currentDirectory);
        new ArrayList<>(Arrays.asList(Objects.requireNonNull(f.list()))).forEach(logger::info);

    }

    public void setCurrentDirectory(String path) {
        this.currentDirectory = path;
    }

    private Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DisplayDirectoryContent.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
