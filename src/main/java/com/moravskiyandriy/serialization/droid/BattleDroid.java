package com.moravskiyandriy.serialization.droid;

import java.io.Serializable;

public class BattleDroid extends Droid implements Serializable {
    transient private int defence;

    public BattleDroid(final int hp, final int attack, final int defence) {
        super(hp, attack);
        this.defence = defence;
    }

    @Override
    public String toString() {
        return "BattleDroid{" +
                "hp=" + getHp() +
                ", attack=" + getAttack() +
                ", defence=" + defence +
                '}';
    }
}