package com.moravskiyandriy.serialization.droid;

import java.io.Serializable;

public class Droid implements Serializable {
    private int hp;
    private int attack;

    public Droid() {
    }

    public Droid(final int hp, final int attack) {
        this.hp = hp;
        this.attack = attack;
    }

    int getHp() {
        return hp;
    }

    public void setHp(final int hp) {
        this.hp = hp;
    }

    int getAttack() {
        return attack;
    }

    public void setAttack(final int attack) {
        this.attack = attack;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "hp=" + hp +
                ", attack=" + attack +
                '}';
    }
}
