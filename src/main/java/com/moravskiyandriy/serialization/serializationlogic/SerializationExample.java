package com.moravskiyandriy.serialization.serializationlogic;

import com.moravskiyandriy.serialization.droid.BattleDroid;
import com.moravskiyandriy.serialization.droid.Droid;
import com.moravskiyandriy.serialization.genericship.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Optional;
import java.util.Properties;

public class SerializationExample {
    private static final Logger logger = LogManager.getLogger(SerializationExample.class);
    private static final String SERIALIZATION_FILE_NAME = "ship.ser";

    public void serializeDeserialize() {
        Properties prop = getProperties();

        Ship<Droid> ship = getDroidShip();

        String filename = Optional.ofNullable(prop.
                getProperty("SERIALIZATION_FILE_NAME")).
                orElse(SERIALIZATION_FILE_NAME);

        serialize(ship, filename);

        Ship<Droid> ship1 = Optional.ofNullable(deserialize(filename)).orElse(new Ship<>());

        logger.info("Ship:");
        ship1.getDroids().forEach(logger::info);
    }

    private Ship<Droid> deserialize(String filename) {
        Ship<Droid> ship1 = null;

        try {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);
            ship1 = (Ship<Droid>) in.readObject();
            in.close();
            file.close();

        } catch (IOException e) {
            logger.info("IOException is caught:");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            logger.info("ClassNotFoundException is caught.");
        }
        logger.info("Object has been deserialized.");
        return ship1;
    }

    private void serialize(Ship<Droid> ship, String filename) {
        try {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(ship);
            out.close();
            file.close();
            logger.info("Object has been serialized.");

        } catch (IOException e) {
            logger.info("IOException is caught:");
            e.printStackTrace();
        }
    }

    private Ship<Droid> getDroidShip() {
        Ship<Droid> ship = new Ship<>();
        ship.add(new Droid(20, 5));
        ship.add(new Droid(23, 6));
        ship.add(new Droid(25, 5));
        ship.add(new BattleDroid(28, 8, 6));
        ship.add(new BattleDroid(21, 5, 3));
        return ship;
    }

    private Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = SerializationExample.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
