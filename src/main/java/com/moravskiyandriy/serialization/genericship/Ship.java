package com.moravskiyandriy.serialization.genericship;

import com.moravskiyandriy.serialization.droid.Droid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droid> implements Serializable {
    private static final Logger logger = LogManager.getLogger(Ship.class);
    List<T> ship;

    public Ship() {
        ship = new ArrayList<T>();
    }

    public void add(final T droid) {
        ship.add(droid);
    }

    public List<T> getDroids() {
        return ship;
    }

    public void showDroids(final List<? extends T> ship) {
        for (T droid : ship) {
            logger.info(droid);
        }
    }
}
