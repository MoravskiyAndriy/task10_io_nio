package com.moravskiyandriy.commentsreader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Stream;

public class CommentsReader {
    private static final Logger logger = LogManager.getLogger(CommentsReader.class);
    private static final String COMMENTS_FILE_NAME = "Comments.java";
    private String text;

    public void ReadComments() {
        Properties prop = getProperties();
        try {
            FileReader in = new FileReader(Optional.ofNullable(prop.
                    getProperty("COMMENTS_FILE_NAME")).
                    orElse(COMMENTS_FILE_NAME));
            BufferedReader br = new BufferedReader(in);

            String line;
            boolean massCommentflag = false;
            boolean docCommentflag = false;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                char[] arr = line.toCharArray();
                if (massCommentflag) {
                    if (arr.length > 2 && arr[arr.length - 2] != '*' && arr[arr.length - 1] != '/') {
                        text += line;
                        text += "\n";
                        continue;
                    } else if (arr.length == 1) {
                        text += arr[0];
                        text += "\n";
                    } else {
                        text += line;
                        massCommentflag = false;
                        text += "\n";
                    }
                }
                if (docCommentflag) {
                    if (arr.length > 2 && arr[arr.length - 2] != '*' && arr[arr.length - 1] != '/') {
                        text += line;
                        text += "\n";
                        continue;
                    } else if (arr.length == 1) {
                        text += arr[0];
                        text += "\n";
                    } else {
                        docCommentflag = false;
                        text += line + "\n";
                    }
                }
                if (arr.length > 2 && arr[0] == '/' && arr[1] == '/') {
                    Stream.iterate(0, n -> n + 1)
                            .limit(arr.length)
                            .forEach(x -> text += (char) arr[x]);
                    text += "\n";
                } else if (arr.length > 2 && arr[0] == '/' && arr[1] == '*' && arr[2] == '*') {
                    text += line + "\n";
                    docCommentflag = true;
                } else if (arr.length > 2 && arr[0] == '/' && arr[1] == '*') {
                    Stream.iterate(0, n -> n + 1)
                            .limit(arr.length)
                            .forEach(x -> text += (char) arr[x]);
                    text += "\n";
                    massCommentflag = true;
                }
            }
        } catch (FileNotFoundException e) {
            logger.info("File not found.");
        } catch (IOException e) {
            logger.info("IO exception found:");
            e.printStackTrace();
        }
        logger.info(text);
    }

    private Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = CommentsReader.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
